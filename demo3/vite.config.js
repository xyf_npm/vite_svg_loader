import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { svgBuilder } from "vite-svg-icon";
// const { svgBuilder } = require("../index");
const root = process.cwd();

export default defineConfig({
  root,
  base: "./",
  server: {
    host: "0.0.0.0",
  },

  build: {
    target: "es2015",

  },

  plugins: [
    svgBuilder("./aeeset/svg/"),
    vue(),
  ],
});
