import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import svgIcon from './components/svgIcon.vue';

const app = createApp(App);
app.component('svg-icon', svgIcon);

app.mount('#app');
